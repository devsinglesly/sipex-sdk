import axios, { AxiosInstance } from 'axios';
import { NetworkService } from './services/network.service';
import { TokenService } from './services/token.service';
import { AuthService } from './services/auth.service';
import { ApplicationService } from './services/application.service';
import { WalletService } from './services/wallet.service';
import { WalletBalanceService } from './services/wallet-balance.service';
import { Application } from './interfaces/application';
import { BehaviorSubject, Observable } from 'rxjs';
import { HeadersEnum } from './enums/headers.enum';
import { CallerTypeEnum } from './enums/caller-type.enum';
import {BridgeService} from "./services/bridge.service";

export interface BridgeClientConfiguration {
  storageKeysPrefix?: string;
  storage?: Storage;
}

export type SupportedServices =
  | AuthService
  | ApplicationService
  | WalletService
  | NetworkService
  | TokenService
  | WalletBalanceService
  | BridgeService;

export class BridgeClient {
  private readonly http: AxiosInstance;
  private readonly storage: Storage;
  private readonly instanceServices = new Map();
  private application$: BehaviorSubject<Application | undefined> =
    new BehaviorSubject<Application | undefined>(undefined);

  constructor(private readonly config: BridgeClientConfiguration = {}) {
    this.http = axios.create();
    this.storage = this.config.storage ?? localStorage;
    this.config.storageKeysPrefix = this.config.storageKeysPrefix ?? 'bridge';
    this.http.interceptors.request.use((config) => {
      const token = this.storage.getItem(
        `${this.config.storageKeysPrefix}_token`,
      );
      config.headers.set('Authorization', `Bearer ${token}`);

      if (
        config.headers.get(HeadersEnum.X_CALLER_TYPE) ===
        CallerTypeEnum.APPLICATION
      ) {
        const application = this.application$.getValue();
        if (application) {
          config.auth = {
            username: application.authId,
            password: application.secretKey,
          };
        }
      }

      return config;
    });
  }

  public getService<T extends { new (...args): SupportedServices }>(
    s: T,
  ): InstanceType<T> {
    if (this.instanceServices.has(s)) {
      return this.instanceServices.get(s);
    }

    this.instanceServices.set(s, new s(this.http, this.storage, this.config));

    return this.getService(s);
  }

  public useApplication(application: Application | undefined): this {
    this.application$.next(application);
    return this;
  }

  public getSelectedApplication$(): Observable<Application | undefined> {
    return this.application$;
  }
}
