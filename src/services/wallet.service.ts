import { Service } from './service';
import { Wallet } from '../interfaces/wallet';

export class WalletService extends Service {
  public async getWallets(): Promise<Wallet[]> {
    return this.http.get('/api/bridge/wallets').then((res) => res.data);
  }
}
