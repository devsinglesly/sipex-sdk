import { AxiosInstance } from 'axios';
import { Token } from '../interfaces/token';
import { TokenTypeEnum } from '../enums/token-type.enum';

export interface CreateTokenOptions {
  symbol: string;
  decimals: number;
  networkCode: string;
  type: TokenTypeEnum;
  contractAddress: '' | string;
}

export class TokenService {
  constructor(private readonly http: AxiosInstance) {}

  public getTokens(): Promise<Token[]> {
    return this.http.get(`/api/tokens`).then((res) => res.data);
  }

  public create(options: CreateTokenOptions): Promise<Token> {
    return this.http.post(`/api/token`, options).then((res) => res.data);
  }

  public delete(symbol: string): Promise<void> {
    return this.http.delete(`/api/token/${symbol}`);
  }
}
