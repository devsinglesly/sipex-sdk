import { AxiosInstance } from 'axios/index';
import { BridgeClientConfiguration } from '../bridge-client';

export abstract class Service {
  constructor(
    protected readonly http: AxiosInstance,
    protected readonly storage: Storage,
    protected readonly config: BridgeClientConfiguration,
  ) {}
}
