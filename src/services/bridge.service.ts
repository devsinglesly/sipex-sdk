import { Service } from './service';
import { Balance } from '../interfaces/balance';
import { HeadersEnum } from '../enums/headers.enum';
import { CallerTypeEnum } from '../enums/caller-type.enum';
import { TransferOptions } from '../interfaces/transfer.options';

export class BridgeService extends Service {
  public async getBalance(
    network: string,
    symbol: string,
    address: string,
  ): Promise<Balance> {
    return this.http
      .get<Balance>(
        `/api/bridge/${network}/${symbol}/wallet/${address}/balance`,
        {
          headers: {
            [HeadersEnum.X_CALLER_TYPE]: CallerTypeEnum.APPLICATION,
          },
        },
      )
      .then((res) => res.data);
  }

  public async transfer(
    options: TransferOptions,
  ): Promise<{ transactionId: string }> {
    return this.http
      .post(
        `/api/bridge/${options.network}/${options.symbol}/transfer`,
        {
          from: options.from,
          to: options.to,
          amount: options.amount,
        },
        {
          headers: {
            [HeadersEnum.X_CALLER_TYPE]: CallerTypeEnum.APPLICATION,
          },
        },
      )
      .then((res) => res.data);
  }
}
