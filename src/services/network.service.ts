import { AxiosInstance } from 'axios';
import { Network, NetworkOptions } from '../interfaces/network';
import { NetworkKindEnum } from '../enums/network-kind.enum';

export interface CreateNetworkRequest {
  code: string;
  name: string;
  kind: string;
  url: string;
  apiKey: string;
  username: string;
  password: string;
}

export class NetworkService {
  constructor(private readonly http: AxiosInstance) {}

  public getNetworks(): Promise<Network[]> {
    return this.http.get('/api/networks').then((res) => res.data);
  }

  public create(options: NetworkOptions): Promise<Network> {
    const request = {
      name: options.name,
      code: options.code,
      kind: options.kind,
      url: options.url,
    } as CreateNetworkRequest;

    if (options.kind === NetworkKindEnum.TRON_COMPATIBLE) {
      request.apiKey = options.apiKey;
    }

    return this.http.post(`/api/network`, request);
  }

  public delete(code: string): Promise<void> {
    return this.http.delete(`/api/network/${code}`);
  }
}
