import { Service } from './service';
import { WalletWithBalances } from '../interfaces/wallet-with-balances';

export class WalletBalanceService extends Service {
  public async getWalletsWithBalances(): Promise<WalletWithBalances[]> {
    return this.http.get('/api/wallet-balances').then((res) => res.data);
  }
}
