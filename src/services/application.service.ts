import { AxiosInstance } from 'axios';
import { BridgeClientConfiguration } from '../bridge-client';
import { Service } from './service';
import { Application } from '../interfaces/application';

export class ApplicationService extends Service {
  public async create(name: string): Promise<Application> {
    return this.http.post('/api/application').then((res) => res.data);
  }

  public async getApplications(): Promise<Application[]> {
    return this.http.get('/api/applications').then((res) => res.data);
  }

  public async exportPrivateKey(password: string): Promise<string> {
    // TODO decipher private key
    return 'secret key';
  }

  public async resetPrivateKey(password: string): Promise<void> {
    // TODO reset private key
  }
}
