export interface TransferOptions {
  network: string;
  symbol: string;
  from: string;
  to: string;
  amount: string;
}
