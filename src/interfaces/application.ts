export interface Application {
  readonly name: string;
  readonly authId: string;
  readonly secretKey: string;
  readonly id: string;
}
