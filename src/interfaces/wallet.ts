export interface Wallet {
  address: string;
  network: string;
  owners: string[];
  type: 'MAIN';
  createdAt: string;
}
