export interface Balance {
  amount: string;
  decimals: number;
}
