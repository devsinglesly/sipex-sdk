import { Network } from './network';
import { TokenTypeEnum } from '../enums/token-type.enum';

export interface Token {
  symbol: Lowercase<string>;
  decimals: number;
  network: Network;
  type: TokenTypeEnum;
  contractAddress: '' | string;
  id: string;
}
