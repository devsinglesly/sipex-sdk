export interface WalletWithBalances {
  address: string;
  balances: Record<string, { amount: string; decimals: number }>;
  network: string;
  owners: string[];
}
