export interface Account {
  address: string;
  id: string;
  createdAt: string;
}
